import * as flsFunctions from './modules/functions.js';

flsFunctions.isWebp();
const monthlyRadio = document.getElementById("monthly");
const annualRadio = document.getElementById("annual");
const monthlyPrice = document.querySelectorAll(".cards__pricing-monthly");
const annualPrice = document.querySelectorAll(".cards__pricing-annual");

monthlyRadio.addEventListener("change", () => {
   if (monthlyRadio.checked) {
      monthlyPrice.forEach((price) => {
         console.log(price.style);
         price.style.display = "inline-block"
      })
      annualPrice.forEach((price) => {
         console.log(price.style);
         price.style.display = "none"
      })
   }
});

annualRadio.addEventListener("change", () => {
   if (annualRadio.checked) {
      annualPrice.forEach((price) => {
         console.log(price.style);
         price.style.display = "inline-block"
      })
      monthlyPrice.forEach((price) => {
         console.log(price.style);
         price.style.display = "none"
      })
   }
});